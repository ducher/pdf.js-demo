// Load the http module to create an http server.
var http = require('http');
var url = require('url');
var fs = require('fs');

//
// implementing endsWith because V8 doesn't support it
// Returns true if the String ends with the String we gave it
// See https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/endsWith
//
String.prototype.endsWith = function(suffix) {
    return this.indexOf(suffix, this.length - suffix.length) !== -1;
};

// Configure our HTTP server to respond with Hello World to all requests.
var server = http.createServer(function (request, response) {

  var pathname = url.parse(request.url).pathname;
  var data;
  var mime;
  
  console.log ("Received request for path "+pathname);
  
  switch (pathname) {
    case '/':
      data = fs.readFileSync('pdfjs_demo.html');
      mime = 'text/html;';
      break;
      
    default:
      data = fs.readFileSync('.'+pathname);
      
      if (pathname.endsWith('.pdf')) {
        mime = 'application/pdf;';
      } else if (pathname.endsWith('.js')) {
        mime = 'text/plain;'
      }
      break;
  }
  
  response.writeHead(200, {'Content-Type': mime+' charset=utf-8'});
  response.end(data);





});


// Listen on port 8000, IP defaults to 127.0.0.1
server.listen(8001);

// Put a friendly message on the terminal
console.log("Server running at http://127.0.0.1:8001/");
