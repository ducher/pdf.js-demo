#About

Proof of concept that pdf.js is easy to use and we can easily track the current page's number.

#How to use it

Install node.js if you don't already have it. Run 

	$ node node\_server.js

and go to http://127.0.0.1:8001/.

This will just display a PDF course about C++ classes, you can go to the next slide by pressing any key on your keyboard and you can see on which slide you are beneath the slide.

This has been tested in Firefox, but it won't work in Chrome (webworkers I believe).

#Credits

The original demo (pdfjs helloworld) is available in the basic_demo.html file and [online here](http://jsbin.com/pdfjs-helloworld-v2/1/edit). The PDF file (beamer-tutorial.pdf) copyright belongs to the MIT, I do not own it and it will be taken down if I'm asked to.
